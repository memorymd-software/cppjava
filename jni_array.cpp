#include "jni_array.h"
#include "jni_exceptions.h"

namespace Neuro::Ble::Impl::Jni
{
	jsize get_array_size(JNIEnv* env, jarray java_array) noexcept
	{
		if (java_array == nullptr)
			return 0;
		try 
		{
			return call_and_handle_java_exceptions(env, &JNIEnv::GetArrayLength, java_array);
		}
		catch (...)
		{
			return 0;
		}
	}
}