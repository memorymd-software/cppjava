#ifndef JNI_CLASS_H
#define JNI_CLASS_H

#include "jni_builtin_types.h"
#include "jni_exceptions.h"

namespace Neuro::Ble::Impl::Jni
{
	template <typename T>
	T get_static_field_value(JNIEnv* env, jclass class_object, jfieldID field_id);

	template <>
	jint get_static_field_value<jint>(JNIEnv* env, jclass class_object, jfieldID field_id);

	template <>
	jlong get_static_field_value<jlong>(JNIEnv* env, jclass class_object, jfieldID field_id);

	template <>
	jstring get_static_field_value<jstring>(JNIEnv* env, jclass class_object, jfieldID field_id);

	template <>
	jobject get_static_field_value<jobject>(JNIEnv* env, jclass class_object, jfieldID field_id);

	template <typename T>
	jfieldID get_static_field_id(JNIEnv* env, jclass java_class, const char* field_name)
	{
		return call_and_handle_java_exceptions(env, &JNIEnv::GetStaticFieldID, java_class, field_name, TypeSignatureV<T>);
	}

	template <typename T>
	jfieldID get_static_field_id(JNIEnv* env, jclass java_class, const char* field_name, const char* signature)
	{
		return call_and_handle_java_exceptions(env, &JNIEnv::GetStaticFieldID, java_class, field_name, signature);
	}

	template <typename Ret>
	struct StaticMethodCaller
	{
		template <typename... Args>
		Ret operator()(JNIEnv* env, jclass java_class, jmethodID method_id, Args&&... args);
	};

	template <>
	struct StaticMethodCaller<void>
	{
		template <typename... Args>
		void operator()(JNIEnv* env, jclass java_class, jmethodID method_id, Args&&... args)
		{
			call_helper(env, java_class, method_id, std::forward<Args>(args)...);
		}

		void call_helper(JNIEnv* env, jclass java_class, jmethodID method_id, ...)
		{
			va_list args;
			va_start(args, method_id);
			call_and_handle_java_exceptions(env, &JNIEnv::CallStaticIntMethodV, java_class, method_id, args);
			va_end(args);
		}
	};

	template <>
	struct StaticMethodCaller<jint>
	{
		template <typename... Args>
		jint operator()(JNIEnv* env, jclass java_class, jmethodID method_id, Args&&... args)
		{
			return call_helper(env, java_class, method_id, std::forward<Args>(args)...);
		}

		jint call_helper(JNIEnv* env, jclass java_class, jmethodID method_id, ...)
		{
			va_list args;
			va_start(args, method_id);
			auto result = call_and_handle_java_exceptions(env, &JNIEnv::CallStaticIntMethodV, java_class, method_id, args);
			va_end(args);
			return result;
		}
	};

	template <>
	struct StaticMethodCaller<jlong>
	{
		template <typename... Args>
		jlong operator()(JNIEnv* env, jclass java_class, jmethodID method_id, Args&&... args)
		{
			return call_helper(env, java_class, method_id, std::forward<Args>(args)...);
		}

		jlong call_helper(JNIEnv* env, jclass java_class, jmethodID method_id, ...)
		{
			va_list args;
			va_start(args, method_id);
			auto result = call_and_handle_java_exceptions(env, &JNIEnv::CallStaticLongMethodV, java_class, method_id, args);
			va_end(args);
			return result;
		}
	};

	template <>
	struct StaticMethodCaller<jobject>
	{
		template <typename... Args>
		jobject operator()(JNIEnv* env, jclass java_class, jmethodID method_id, Args&&... args)
		{
			return call_helper(env, java_class, method_id, std::forward<Args>(args)...);
		}

		jobject call_helper(JNIEnv* env, jclass java_class, jmethodID method_id, ...)
		{
			va_list args;
			va_start(args, method_id);
			auto result = call_and_handle_java_exceptions(env, &JNIEnv::CallStaticObjectMethodV, java_class, method_id, args);
			va_end(args);
			return result;
		}
	};

	jmethodID get_static_method_id(JNIEnv* env, jclass java_class, const char* method_name, const char* signature);

	jclass find_java_class(JNIEnv* env, const char* java_class_path);

	struct JavaClass final
	{
		constexpr explicit JavaClass(jclass java_class) noexcept :
			mJavaClass(java_class)
		{}

		JavaClass(JNIEnv *env, const char *java_class_path) :
			mJavaClass(find_java_class(env, java_class_path))
		{}

		operator jclass() const noexcept
		{
			return mJavaClass;
		}

		template <typename T>
		T getStaticFieldValue(JNIEnv* env, const char* field_name) const
		{
			try
			{
				auto fieldId = get_static_field_id<T>(env, mJavaClass, field_name);
				return get_static_field_value<T>(env, mJavaClass, fieldId);
			}
			catch (std::exception & e)
			{
				throw std::runtime_error(std::string("Unable to get static field value: ") + e.what());
			}
			catch (...)
			{
				throw std::runtime_error("Unable to get static field value");
			}
		}

		template <typename T>
		T getStaticFieldValue(JNIEnv* env, const char* field_name, const char* signature) const
		{
			try
			{
				auto fieldId = get_static_field_id<jobject>(env, mJavaClass, field_name, signature);
				return get_static_field_value<jobject>(env, mJavaClass, fieldId);
			}
			catch (std::exception & e)
			{
				throw std::runtime_error(std::string("Unable to get static field value: ") + e.what());
			}
			catch (...)
			{
				throw std::runtime_error("Unable to get static field value");
			}
		}

		template <typename Ret, typename... Args>
		Ret callStaticMethod(JNIEnv* env, const char* method_name, const char* signature, Args&&... args) const
		{
			try
			{
				auto methodID = get_static_method_id(env, mJavaClass, method_name, signature);
				return StaticMethodCaller<Ret>()(env, mJavaClass, methodID, std::forward<Args>(args)...);
			}
			catch (std::exception & e)
			{
				throw std::runtime_error(std::string("Unable to call static method ") + method_name + " with signature " + signature + ": " + e.what());
			}
			catch (...)
			{
				throw std::runtime_error(std::string("Unable to call static method ") + method_name + " with signature " + signature);
			}
		}

	private:
		jclass mJavaClass;
	};
}

#endif //JNI_CLASS_H