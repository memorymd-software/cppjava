#ifndef JNI_GLOBAL_REF_H
#define JNI_GLOBAL_REF_H

#include "jni.h"
#include "jni_object.h"
#include "jni_exceptions.h"
#include <functional>
#include <exception>
#include <string>

namespace Neuro::Ble::Impl::Jni
{
	JavaVM* get_java_vm(JNIEnv* env);

	struct AttachedEnvironment final
	{
		explicit AttachedEnvironment(JavaVM* jvm) :
			mJavaVM(jvm),
			mNeedDetach(false)
		{
			auto getEnvStat = mJavaVM->GetEnv(reinterpret_cast<void**>(&mEnv), JNI_VERSION_1_6);
			if (getEnvStat == JNI_EDETACHED) {
				if (mJavaVM->AttachCurrentThread(&mEnv, nullptr) != 0) {
					throw std::runtime_error("Unable to attach thread to JVM");
				}
				mNeedDetach = true;
			}
		}

		~AttachedEnvironment()
		{
			if (mNeedDetach)
			{
				mJavaVM->DetachCurrentThread();
			}
		}

		operator JNIEnv* () const noexcept
		{
			return mEnv;
		}

	private:
		JavaVM* mJavaVM;
		bool mNeedDetach;
		JNIEnv* mEnv;
	};
	
	template <typename Callable, typename... Args>
	auto call_in_attached_env(JavaVM* jvm, Callable callable, Args&&... args)
	{
		AttachedEnvironment env{ jvm };
		return callable(env, std::forward<Args>(args)...);
	}
	
	template <typename JavaType>
	jobject create_global_ref(JNIEnv* env, JavaType object)
	{
		try
		{
			return call_and_handle_java_exceptions(env, &JNIEnv::NewGlobalRef, static_cast<jobject>(object));
		}
		catch (std::exception &e)
		{
			throw std::runtime_error(std::string("Cannot create global ref: ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Cannot create global ref");
		}
	}

	template <typename JavaType>
	void destroy_global_ref(JNIEnv* env, JavaType object)
	{
		try
		{
			call_and_handle_java_exceptions(env, &JNIEnv::DeleteGlobalRef, static_cast<jobject>(object));
		}
		catch (std::exception & e)
		{
			throw std::runtime_error(std::string("Cannot delete global ref: ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Cannot delete global ref");
		}
	}
	
	template <typename JavaType>
	class GlobalRef final
	{
	public:
		GlobalRef(JNIEnv *env, JavaType object):
			mJavaVM(get_java_vm(env)),
			mJavaObject(create_global_ref(env, object))
		{}

		GlobalRef(const GlobalRef &rhs):
			mJavaVM(rhs.mJavaVM),
			mJavaObject(call_in_attached_env(mJavaVM, &create_global_ref<JavaType>, rhs.mJavaObject))
		{}

		GlobalRef& operator=(const GlobalRef& rhs)
		{
			GlobalRef temp(rhs);
			swap(temp);
			return *this;
		}

		GlobalRef(GlobalRef &&rhs) noexcept :
			mJavaVM(std::move(rhs.mJavaVM)),
			mJavaObject(std::move(rhs.mJavaObject))
		{}

		GlobalRef& operator=(GlobalRef &&rhs) noexcept
		{
			swap(rhs);
			return *this;
		}
		
		~GlobalRef()
		{
			try
			{
				call_in_attached_env(mJavaVM, &destroy_global_ref<JavaType>, mJavaObject);
			}
			catch(...)
			{
				//do nothing
			}
		}

		void swap(GlobalRef<JavaType> &other) noexcept
		{
			using std::swap;
			swap(mJavaVM, other.mJavaVM);
			swap(mJavaObject, other.mJavaObject);
		}

		JavaType& operator*() const noexcept
		{
			return mJavaObject;
		}

		JavaType* operator->() const noexcept
		{
			return &mJavaObject;
		}
		
	private:
		JavaVM *mJavaVM;
		mutable JavaType mJavaObject;
	};

	template <typename JavaType>
	void swap(GlobalRef<JavaType>& lhs, GlobalRef<JavaType>& rhs) noexcept
	{
		lhs.swap(rhs);
	}
}

#endif //JNI_GLOBAL_REF_H