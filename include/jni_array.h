#ifndef JNI_ARRAY_H
#define JNI_ARRAY_H

#include "jni_exceptions.h"
#include <algorithm>
#include <vector>

namespace Neuro::Ble::Impl::Jni
{
	template <typename T>
	struct java_array_helper;

	template<>
	struct java_array_helper<jobject>
	{
		using array_type = jobjectArray;
		using value_type = jobject;
	};

	template<>
	struct java_array_helper<jboolean>
	{
		using array_type = jbooleanArray;
		using value_type = jboolean;

		static value_type * get_raw_array(JNIEnv* env, array_type array)
		{
			return call_and_handle_java_exceptions(env, &JNIEnv::GetBooleanArrayElements, array, static_cast<jboolean *>(nullptr));
		}

		static void release_raw_array(JNIEnv* env, array_type array, value_type *raw_array)
		{
			call_and_handle_java_exceptions(env, &JNIEnv::ReleaseBooleanArrayElements, array, raw_array, JNI_ABORT);
		}

		static void save_and_release_raw_array(JNIEnv* env, array_type array, value_type* raw_array)
		{
			call_and_handle_java_exceptions(env, &JNIEnv::ReleaseBooleanArrayElements, array, raw_array, 0);
		}
	};

	template<>
	struct java_array_helper<jbyte>
	{
		using array_type = jbyteArray;
		using value_type = jbyte;

		static array_type create_raw_array(JNIEnv* env, jsize size)
		{
			return call_and_handle_java_exceptions(env, &JNIEnv::NewByteArray, size);
		}
		
		static value_type* get_raw_array(JNIEnv* env, array_type array)
		{
			return call_and_handle_java_exceptions(env, &JNIEnv::GetByteArrayElements, array, static_cast<jboolean*>(nullptr));
		}
		
		static void release_raw_array(JNIEnv* env, array_type array, value_type* raw_array)
		{
			call_and_handle_java_exceptions(env, &JNIEnv::ReleaseByteArrayElements, array, raw_array, JNI_ABORT);
		}

		static void save_and_release_raw_array(JNIEnv* env, array_type array, value_type* raw_array)
		{
			call_and_handle_java_exceptions(env, &JNIEnv::ReleaseByteArrayElements, array, raw_array, 0);
		}
	};

	template<>
	struct java_array_helper<jchar>
	{
		using array_type = jcharArray;
		using value_type = jchar;
	};

	template<>
	struct java_array_helper<jshort>
	{
		using array_type = jshortArray;
		using value_type = jshort;
	};

	template<>
	struct java_array_helper<jint>
	{
		using array_type = jintArray;
		using value_type = jint;
	};

	template<>
	struct java_array_helper<jlong>
	{
		using array_type = jlongArray;
		using value_type = jlong;
	};

	template<>
	struct java_array_helper<jfloat>
	{
		using array_type = jfloatArray;
		using value_type = jfloat;
	};

	template<>
	struct java_array_helper<jdouble>
	{
		using array_type = jdoubleArray;
		using value_type = jdouble;

		static array_type create_raw_array(JNIEnv* env, jsize size)
		{
			return call_and_handle_java_exceptions(env, &JNIEnv::NewDoubleArray, size);
		}

		static value_type* get_raw_array(JNIEnv* env, array_type array)
		{
			return call_and_handle_java_exceptions(env, &JNIEnv::GetDoubleArrayElements, array, static_cast<jboolean*>(nullptr));
		}

		static void release_raw_array(JNIEnv* env, array_type array, value_type* raw_array)
		{
			call_and_handle_java_exceptions(env, &JNIEnv::ReleaseDoubleArrayElements, array, raw_array, JNI_ABORT);
		}

		static void save_and_release_raw_array(JNIEnv* env, array_type array, value_type* raw_array)
		{
			call_and_handle_java_exceptions(env, &JNIEnv::ReleaseDoubleArrayElements, array, raw_array, 0);
		}
	};


	template <typename T>
	using java_array_t = typename java_array_helper<T>::array_type;

	jsize get_array_size(JNIEnv* env, jarray) noexcept;

	template <typename T, typename InputIterator>
	java_array_t<T> create_java_array(JNIEnv* env, InputIterator begin, size_t count)
	{
		auto javaArray = java_array_helper<T>::create_raw_array(env, static_cast<jsize>(count));
		auto rawArray = java_array_helper<T>::get_raw_array(env, javaArray);
		for (size_t i = 0; i < count; ++i, ++begin)
		{
			rawArray[i] = static_cast<T>(*begin);
		}
		java_array_helper<T>::save_and_release_raw_array(env, javaArray, rawArray);
		return javaArray;
	}

	template <typename T>
	struct JavaArray final
	{
		using value_type = typename java_array_helper<T>::value_type;
		using size_type = jsize;
		using difference_type = jsize;
		using reference = value_type&;
		using const_reference = const value_type &;

		constexpr explicit JavaArray(java_array_t<T> java_array) noexcept :
			mJavaArray(java_array)
		{}

		template <typename InputIterator>
		JavaArray(JNIEnv* env, InputIterator begin, size_t count) :
			mJavaArray(create_java_array<T>(env, begin, count))
		{}
		
		[[nodiscard]]
		size_type size(JNIEnv* env) const noexcept
		{
			return get_array_size(env, mJavaArray);
		}

		java_array_t<T> javaArray() const noexcept
		{
			return mJavaArray;
		}
		
		std::vector<T> toVector(JNIEnv* env) const
		{
			auto rawArray = java_array_helper<T>::get_raw_array(env, mJavaArray);
			auto rawArraySize = size(env);
			std::vector<T> result(rawArraySize);
			std::copy_n(rawArray, rawArraySize, result.begin());
			java_array_helper<T>::release_raw_array(env, mJavaArray, rawArray);
			return result;
		}

	private:
		java_array_t<T> mJavaArray;
	};

	template <>
	struct JavaArray<jobject> final
	{
		using value_type = java_array_helper<jobject>::value_type;
		using size_type = jsize;
		using difference_type = jsize;

		constexpr explicit JavaArray(java_array_t<jobject> java_array) noexcept :
			mJavaArray(java_array)
		{}

		[[nodiscard]]
		size_type size(JNIEnv* env) const noexcept
		{
			return get_array_size(env, mJavaArray);
		}

		jobject at(JNIEnv *env, size_type index) const
		{
			return call_and_handle_java_exceptions(env, &JNIEnv::GetObjectArrayElement, mJavaArray, index);
		}

	private:
		java_array_t<jobject> mJavaArray;
	};
}

#endif //JNI_ARRAY_H