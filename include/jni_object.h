#ifndef JNI_OBJECT_H
#define JNI_OBJECT_H

#include "jni_class.h"
#include "jni_exceptions.h"

namespace Neuro::Ble::Impl::Jni
{
	template <typename Ret>
	struct MethodCaller
	{
		template <typename... Args>
		Ret operator()(JNIEnv* env, jobject object, jmethodID method_id, Args&&... args);
	};

	template <>
	struct MethodCaller<void>
	{
		template <typename... Args>
		void operator()(JNIEnv* env, jobject object, jmethodID method_id, Args&&... args)
		{
			return call_helper(env, object, method_id, std::forward<Args>(args)...);
		}

		void call_helper(JNIEnv* env, jobject object, jmethodID method_id, ...)
		{
			va_list args;
			va_start(args, method_id);
			call_and_handle_java_exceptions(env, &JNIEnv::CallVoidMethodV, object, method_id, args);
			va_end(args);
		}
	};

	template <>
	struct MethodCaller<jint>
	{
		template <typename... Args>
		jint operator()(JNIEnv* env, jobject object, jmethodID method_id, Args&&... args)
		{
			return call_helper(env, object, method_id, std::forward<Args>(args)...);
		}

		jint call_helper(JNIEnv* env, jobject object, jmethodID method_id, ...)
		{
			va_list args;
			va_start(args, method_id);
			auto result = call_and_handle_java_exceptions(env, &JNIEnv::CallIntMethodV, object, method_id, args);
			va_end(args);
			return result;
		}
	};

	template <>
	struct MethodCaller<jboolean>
	{
		template <typename... Args>
		jboolean operator()(JNIEnv* env, jobject object, jmethodID method_id, Args&&... args)
		{
			return call_helper(env, object, method_id, std::forward<Args>(args)...);
		}

		jboolean call_helper(JNIEnv* env, jobject object, jmethodID method_id, ...)
		{
			va_list args;
			va_start(args, method_id);
			auto result = call_and_handle_java_exceptions(env, &JNIEnv::CallBooleanMethodV, object, method_id, args);
			va_end(args);
			return result;
		}
	};

	template <>
	struct MethodCaller<jlong>
	{
		template <typename... Args>
		jlong operator()(JNIEnv* env, jobject object, jmethodID method_id, Args&&... args)
		{
			return call_helper(env, object, method_id, std::forward<Args>(args)...);
		}

		jlong call_helper(JNIEnv* env, jobject object, jmethodID method_id, ...)
		{
			va_list args;
			va_start(args, method_id);
			auto result = call_and_handle_java_exceptions(env, &JNIEnv::CallLongMethodV, object, method_id, args);
			va_end(args);
			return result;
		}
	};

	template <>
	struct MethodCaller<jdouble>
	{
		template <typename... Args>
		jdouble operator()(JNIEnv* env, jobject object, jmethodID method_id, Args&&... args)
		{
			return call_helper(env, object, method_id, std::forward<Args>(args)...);
		}

		jdouble call_helper(JNIEnv* env, jobject object, jmethodID method_id, ...)
		{
			va_list args;
			va_start(args, method_id);
			auto result = call_and_handle_java_exceptions(env, &JNIEnv::CallDoubleMethodV, object, method_id, args);
			va_end(args);
			return result;
		}
	};

	template <>
	struct MethodCaller<jobject>
	{
		template <typename... Args>
		jobject operator()(JNIEnv* env, jobject object, jmethodID method_id, Args&&... args)
		{
			return call_helper(env, object, method_id, std::forward<Args>(args)...);
		}

		jobject call_helper(JNIEnv* env, jobject object, jmethodID method_id, ...)
		{
			va_list args;
			va_start(args, method_id);
			auto result = call_and_handle_java_exceptions(env, &JNIEnv::CallObjectMethodV, object, method_id, args);
			va_end(args);
			return jobject(result);
		}
	};

	jmethodID get_method_id(JNIEnv* env, jclass java_class, const char* method_name, const char* signature);

	jclass get_object_class(JNIEnv* env, jobject object);

	jobject create_java_object_var(JNIEnv* env, jclass java_class, jmethodID constructor_id, ...);

	template <typename... Args>
	jobject create_java_object(JNIEnv* env, const JavaClass& java_class, const char* constructor_signature, Args&&... args)
	{
		try
		{
			jmethodID constructorID = get_method_id(env, java_class, "<init>", constructor_signature);			
			return create_java_object_var(env, java_class, constructorID, std::forward<Args>(args)...);
		}
		catch (std::exception &e)
		{
			throw std::runtime_error(std::string("Unable to create java object: ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Unable to create java object");
		}
	}

	struct JavaObject final
	{
		constexpr explicit JavaObject(jobject java_object) noexcept :
			mJavaObject(java_object)
		{}

		template <typename... Args>
		JavaObject(JNIEnv *env, const JavaClass &java_class, const char* constructor_signature, Args&&... args):
			mJavaObject(create_java_object(env, java_class, constructor_signature, std::forward<Args>(args)...))
		{}

		template <typename Ret, typename... Args>
		Ret callMethod(JNIEnv* env, const char* method_name, const char* signature, Args&&... args) const 
		{
			try
			{
				auto methodID = get_method_id(env, objectClass(env), method_name, signature);
				return MethodCaller<Ret>()(env, mJavaObject, methodID, std::forward<Args>(args)...);
			}
			catch (std::exception &e)
			{
				throw std::runtime_error(std::string("Unable to call method ") + method_name + " with signature " + signature +": " + e.what());
			}
			catch (...)
			{
				throw std::runtime_error(std::string("Unable to call method ") + method_name + " with signature " + signature);
			}
		}

		JavaClass objectClass(JNIEnv* env) const
		{
			return JavaClass(get_object_class(env, mJavaObject));
		}

		operator jobject() const noexcept
		{
			return mJavaObject;
		}

	private:
		jobject mJavaObject;
	};
}

#endif //JNI_OBJECT_H